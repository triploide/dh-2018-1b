<?php

Route::get('/', function () {
    return view('welcome');
});

//------------------
//-----Ejemplos-----
//------------------
Route::get('ejemplos/eloquent', 'MovieController@index');
Route::get('ejemplos/eloquent/find/{id}', 'MovieController@show');
Route::get('ejemplos/eloquent/edit/{id}', 'MovieController@edit');
Route::get('actores', 'ActorController@index');
Route::get('actor/{id}', 'ActorController@show');

//releaciones
Route::get('tieneMuchos', 'Ejemplos\RelacionesController@tieneMuchos');
Route::get('pertenece', 'Ejemplos\RelacionesController@pertenece');
Route::get('muchosAMuchos', 'Ejemplos\RelacionesController@muchosAMuchos');
Route::get('queries', 'Ejemplos\RelacionesController@queries');

//upload files

//---------------
//-----Front-----
//---------------
Route::get('peliculas', 'MovieController@index');

//---------------
//-----Admin-----
//---------------
Route::get('admin/movies', 'Admin\MovieController@index')->middleware('auth');
Route::get('admin/movies/create', 'Admin\MovieController@create');
Route::post('admin/movies', 'Admin\MovieController@storage')->middleware('getRequest');


Route::get('test-api', function () {
    return '
        <form method="post" action="/api/movie/1?api_token=6a1ZWXX2AOPJrQANMy4uEs6TQiZUcfbra1JD91cZoEuQW5eIJI3CmOuAmbpr">
            <input name="title" value="" />
        </form>
    ';
});






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
