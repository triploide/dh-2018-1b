<!-- blogpost start -->
<article class="blogpost">
    <div class="row grid-space-10">
        <div class="col-md-5">
            <div class="overlay-container">
                <img src="content/peliculas/720x360/ghostbusters.jpg" alt="">
                <a class="overlay-link" href="pelicula"><i class="fa fa-link"></i></a>
            </div>
        </div>
        <div class="col-md-7">
            <header>
                <h2><a href="pelicula">{{ $movie->title }}</a></h2>
                <div class="post-info">
                    <span class="post-date">
                        <i class="icon-calendar"></i>
                        {{ $movie->release_date->format('j-m-Y') }}
                    </span>
                    <span class="comments"><i class="icon-tag-1"></i> <a href="#"
                    >{{ $movie->genre->name }}</a></span>
                </div>
            </header>
            <div class="blogpost-content">
                <p><strong>Premios</strong> {{ $movie->awards }}</p>
            </div>
        </div>
    </div>
</article>
<!-- blogpost end -->