<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Actores</title>
</head>
<body>
    @foreach ($actors as $actor)
        <p><a href="/actor/{{$actor->id}}">{{ $actor->first_name }}</a></p>
    @endforeach
</body>
</html>
