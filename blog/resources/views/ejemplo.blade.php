<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Blade</title>
</head>
<body>

  <h1>Vista blade</h1>
  <p>{{$nombres[4]}} {{ $var2 }}</p>

  @include('archivo') {{-- Sin el .blade.php --}}

  {{--
  <h2>Nombres</h2>
  <ul>
    @foreach ($nombres as $nombre)
      <li>{{ $nombre }}</li>
    @endforeach
  </ul>
  --}}

  <h2>Nombres</h2>
  <ul>
    @forelse ($nombres as $nombre)
      <li>{{ $nombre }}</li>
    @empty
      <p>No hay nombres para mostrar</p>
    @endforelse
  </ul>
{{--
  @if ($nombres[1] == 'Bort')
    <p>El primer nombre el Bort</p>
  @else
    <p>El primer nombre no es Bort</p>
  @endif
  --}}


</body>
</html>
