<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MovieRequest;
use App\Http\Controllers\Controller;
use App\Movie;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::orderBy('title')->with('actors')->get();
        //dd($movies);
        /*
        $moviesTitles = $movies->map(function ($item) {
            return $item->title;
        });
        */
        
        $topMovies = $movies->filter(function ($item) {
            return $item->rating >= 8;
        });
        
        //dd($topMovies->pluck('title')->implode(', '));
        
        
        //return view('admin.movies.index', ['movies' => $movies]);
        return view('admin.movies.index', compact('movies'));
    }
    
    public function create()
    {
        return view('admin/movies/create');
    }
    public function storage()
    {
        
        $nombre = str_slug(request()->input('title'));
        $nombre .= '.' . request()->file('banner')->extension();
        
        if (request()->hasFile('banner')) {
            request()->file('banner')->storeAs('images/movies', $nombre);
        }
        exit;
        
        //dd( request()->all() );
        /*
        $movie = new Movie;
        $movie->title = request()->input('title');
        //$movie->rating = request()->input('rating');
        $movie->awards = request()->input('awards');
        $movie->length = request()->input('length');
        $movie->release_date = request()->input('release_date');
        $movie->save();
        */

        /*
        request()->validate([
            'title' => 'required|max:255',
            'rating' => 'numeric|min:0|max:10',
            'awards' => 'required',
            'release_date' => 'required|date',
        ], [
            'title.required' => 'El título es obligatorio'
        ]);
        */

        Movie::create(request()->all());
    }
}
