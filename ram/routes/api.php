<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|Route::get('movies', function () {
    $movies = \App\Movie::limit(5)->get();
    return $movies->toJson();
});
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('movies', function () {
    $movies = \App\Movie::limit(5)->get();
    return $movies->toJson();
});

Route::post('movie/{id}', function (Request $request, $id) {
    return $request->input('title');
    return \App\Movie::find($id);
})->middleware('auth:api');









