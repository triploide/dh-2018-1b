<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //protected $table = 'movies'; Si mi tabla no respeta el standar

    //public $timestamps = false; //Si no tengo las columnas created_at y updated_at

    protected $fillable = ['title', 'awards', 'length', 'release_date'];
    
    protected $dates = ['release_date'];
    
    public function genre()
    {
        //return $this->belongsTo(Genre::class, 'genre_id', 'id');
        return $this->belongsTo(Genre::class);
    }
    
    public function actors()
    {
        return $this->belongsToMany(Actor::class);
    }
    
    public function getPremiosPorMinuto()
    {
      return $this->awards / $this->length;
    }

}
