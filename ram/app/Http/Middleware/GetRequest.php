<?php

namespace App\Http\Middleware;

use Closure;

class GetRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() == 'GET') {
            return $next($request);
        } else {
            abort(404);
        }
    }
}





