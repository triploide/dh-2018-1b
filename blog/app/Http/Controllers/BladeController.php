<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BladeController extends Controller
{
    public function index()
    {
        $nombres = [
          1 => 'Barry',
          2 => 'Barclay',
          3 => 'Bert',
          4 => 'Bort'
        ];
        return view('ejemplo', ['nombres' => $nombres, 'var2' => 'Bort']);
    }

    public function extender()
    {
      return view('hija');
    }
}
