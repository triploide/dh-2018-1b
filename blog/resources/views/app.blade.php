<!DOCTYPE html>
<html lang="en">
<head>
  {{--<title>@yield('title', 'Home') - Blog</title>--}}
  <title>@yield('title') - Blog</title>
  @include('partials/head')
</head>
<body>
  <header>
    <h1>Header</h1>
  </header>
  <nav>
    <ul>
      <li><a href="#">Uno</a></li>
      <li><a href="#">Dos</a></li>
      <li><a href="#">Tres</a></li>
    </ul>
  </nav>
  <main>
    @yield('content')
  </main>
  <footer>
    Footer
  </footer>
</body>
</html>
