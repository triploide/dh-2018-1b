<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function () {
  return '<h1>Hola Laravel</h1>';
});

Route::get('perfil/{usuario}', function ($usuario) {
  return "<h1>Hola $usuario</h1>";
});

Route::get('home/{usuario?}', function ($usuario='Usuario') {
  return "<h1>Hola $usuario</h1>";
});

Route::get('resultado/{numero1}/{numero2?}', function ($numero1, $numero2=null) {
  if ($numero2 === null) {
    return ($numero1 % 2 == 0) ? 'Es par' : 'No es par';
  } else {
    return $numero1 * $numero2;
  }
});

Route::get('archivo', function () {
  return view('archivo');
});

Route::get('ejemplos/controladores', 'EjemploController@holaMundo');
Route::get('ejemplos/controladores/con-parametro/{param}', 'EjemploController@mostrarParam');

Route::get('personas/{id}', 'EjemploController@buscarPersonas');

Route::get('ejemplos/blade', 'BladeController@index');
Route::get('ejemplos/blade/extension', 'BladeController@extender');
