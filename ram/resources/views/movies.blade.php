<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Películas</title>
</head>
<body>
  <h1>Películas</h1>
  <ul>
    @foreach ($peliculas as $pelicula)
      <li>{{ $pelicula->title }} ({{ $pelicula->length }}) - {{ $pelicula->awards }}</li>
    @endforeach
  </ul>
</body>
</html>