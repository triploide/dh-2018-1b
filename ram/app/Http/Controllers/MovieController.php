<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller
{
    public function index()
    {
        /*
          $movies = \App\Movie::where('rating', '>', 9)
            ->orderBy('title')
            ->get()
          ;
  $movies = \App\Movie::where('release_date', '>=', "2008-01-01")
    ->where('release_date', '<=', '2008-12-31')
    ->orWhere(function ($query) {
      $query->where('rating', '>=', 5)
        ->where('rating', '<=', 7)
      ;
    })
    ->get()
  ;
  
  
        $movies = \App\Movie::where(function($query) {
          $query->where('length', '>=', 120)
            ->where('length', '<=', 150)
          ;
        })->orWhere(function ($query) {
          $query->where('awards', '>=', 2)
            ->where('awards', '<=', 3)
          ;
        })->get();
        */
        
        $movies = Movie::limit(5)->get();
        return view('front.movies.index', ['movies' => $movies]);
    }
    
    public function show($id)
    {
      $movie = \App\Movie::find($id);
      return view('movie', ['movie' => $movie]);
    }
    
    public function edit($id)
    {
      $movie = \App\Movie::find($id);
      dd($movie->getPremiosPorMinuto());
      $movie->rating = 10;
      $movie->save();
      dd($movie->toArray());
      // ?
    }
}
