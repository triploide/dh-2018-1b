<html>
    <head>
        <title>Listado de Películas</title>
        <link rel="stylesheet" href="/css/app.css">
        <style media="screen">
            body {
                padding: 30px
            }
            .page-item.active .page-link {
                background-color: tomato
            }
        </style>
    </head>
    <body>
        @if (\Auth::check())
            <div class="row">
                <div class="col-md-12">
                    <a class="push-right" href="#">
                        {{ \Auth::user()->name }}
                    </a>
                </div>
            </div>
            <br>
        @endif
        
        @auth
            <p>Otra cosa</p>
        @else
            <p>Else del auth</p>
        @endauth
        
        <main class="container">
            <h1>Películas</h1>
            <table class="table table-bordered table striped">
                <thead class="primary">
                    <tr>
                        <th>Título</th>
                        <th>Actores</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($movies as $movie)
                        <tr>
                            <td>{{ $movie->title }}</td>
                            <td>{{ $movie->actors->pluck('first_name')->implode(', ') }}</td>
                            <td>Acciones</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </main>
    </body>
</html>
