<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EjemploController extends Controller
{
    public function holaMundo()
    {
      return "hola desde el controller";
    }

    public function mostrarParam($param)
    {
      return $param;
    }

    public function buscarPersonas($id)
    {
        $nombres = [
          1 => 'Barry',
          2 => 'Barclay',
          3 => 'Bert',
          4 => 'Bort'
        ];
        return (isset($nombres[$id])) ? $nombres[$id] : 'No se encontró el usuario';
    }
}
