<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->input('id');
        return [
            'title' => ['required', 'max:255', Rule::unique('movies')->ignore($id)],
            'rating' => 'numeric|min:0|max:10',
            'awards' => 'required',
            'release_date' => 'required|date',

        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio'
        ];
    }
}
