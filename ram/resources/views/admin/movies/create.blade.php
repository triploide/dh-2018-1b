<html>
    <head>
        <title>Agregar Película</title>
        <link rel="stylesheet" href="/css/app.css">
        <style media="screen">
            body {
                padding: 30px
            }
        </style>
    </head>
    <body>
        <main class="container">
            @if ($errors->any())
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif

            <div class="row">
                <div class="col">
                    <h1>Agregar Película</h1>
                    <form action="/admin/movies" id="agregarPelicula" name="agregarPelicula" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input class="form-control" type="text" name="title" id="titulo" value="{{old('title')}}" />
                        </div>
                        <div class="form-group">
                            <label for="rating">Rating</label>
                            <input class="form-control" type="text" name="rating" id="rating" value="{{old('rating')}}"/>
                        </div>
                        <div class="form-group">
                            <label for="premios">Premios</label>
                            <input class="form-control" type="text" name="awards" id="premios" value="{{old('premios')}}"/>
                        </div>
                        <div class="form-group">
                            <label for="duracion">Duración</label>
                            <input class="form-control" type="text" name="length" id="duracion" value="{{old('duracion')}}"/>
                        </div>
                        <div class="form-group">
                            <label>Fecha de Estreno</label>
                            <input class="form-control" type="date" name="release_date" value="{{old('release_date')}}">
                        </div>
                        <div class="form-group">
                            <label>Banner</label>
                            <input class="form-control" type="file" name="banner">
                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" value="Agregar Pelicula" name="submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </body>
</html>
