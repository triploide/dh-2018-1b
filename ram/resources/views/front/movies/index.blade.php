<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <title>Rent a Movie</title>
        <meta name="description" content="Mirá todas las películas y series desde la comodidad de tu casa. Rent a movie, compartiendo momentos.">
        @include("front/partials/head")
    </head>

    <body class="no-trans front-page">

        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
        
        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">
        
            @include("front/partials/header")
            
            <!-- breadcrumb start -->
            <!-- ================ -->
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home pr-10"></i><a href="/">Home</a></li>
                        <li class="active">Películas</li>
                    </ol>
                </div>
            </div>
            <!-- breadcrumb end -->
        
            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container">

                <div class="container">
                    <div class="row">

                        <!-- main start -->
                        <!-- ================ -->
                        <div class="main col-md-8">

                            <!-- page-title start -->
                            <!-- ================ -->
                            <h1 class="page-title">Películas - Todas</h1>
                            <div class="separator-2"></div>
                            <!-- page-title end -->
                            {{--
                            @foreach ($movies as $movie)
                                @include('front/movies/_movie')
                            @endforeach
                            --}}
                            @each('front/movies/_movie', $movies, 'movie')
                            
                            <!-- pagination start -->
                            <nav>
                                <ul class="pagination">
                                    <li><a href="#" aria-label="Previous"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#" aria-label="Next"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </nav>
                            <!-- pagination end -->

                        </div>
                        <!-- main end -->

                        <!-- sidebar start -->
                        <!-- ================ -->
                        <aside class="col-md-4 col-lg-3 col-lg-offset-1">
                            <div class="sidebar">
                                
                                @include("front/aside/buscar")
                                
                                @include("front/aside/categorias")

                                @include("front/aside/relacionados")
                                                              
                            </div>
                        </aside>
                        <!-- sidebar end -->

                    </div>
                </div>
            </section>
            <!-- main-container end -->

            @include("front/components/calls-to-action/recomendaciones")
            
            @include("front/partials/footer")
            
        </div>
        <!-- page-wrapper end -->

        @include("front/partials/scripts")

    </body>
</html>
