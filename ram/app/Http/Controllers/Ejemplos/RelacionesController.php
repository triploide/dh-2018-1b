<?php

namespace App\Http\Controllers\Ejemplos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelacionesController extends Controller
{
    public function tieneMuchos()
    {
        $genre = \App\Genre::find(7);
        $text = "<h1>".$genre->name."</h1>";
        foreach ($genre->movies as $movie) {
            $text .= "<p>".$movie->title."</p>";
        }
        return $text;
    }
    
    public function muchosAMuchos()
    {
        $movie = \App\Movie::find(2);
        $text = '<h1>'.$movie->title.'</h1>' ;
        foreach ($movie->actors as $actor) {
            $text .= '<p>'.$actor->first_name.'</p>';
        }
        return $text;
    }
    
    public function pertenece()
    {
        $movie = \App\Movie::find(2);
        $text = '<h1>'.$movie->title.'</h1>' ;
        $text .= '<p>'.$movie->genre->name.'</p>' ;
        return $text;
    }
    
    public function queries()
    {
        $movie = \App\Movie::find(2);
        $text = '<h1>'.$movie->title.'</h1>' ;
        $actors = $movie->actors()
            ->where('first_name', 'like', 'B%')
            ->orderBy('first_name')
            ->get()
        ;
        
        foreach ($actors as $actor) {
            $text .= '<p>'.$actor->first_name.'</p>';
        }
        return $text;
    }
}



